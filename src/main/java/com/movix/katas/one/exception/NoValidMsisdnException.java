package com.movix.katas.one.exception;

public class NoValidMsisdnException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NoValidMsisdnException(){
		super()
	}

	public NoValidMsisdnException(String msg) {
		super(msg)
	}
}
