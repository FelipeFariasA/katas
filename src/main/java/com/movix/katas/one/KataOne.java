package com.movix.katas.one;

import com.movix.katas.one.exception.NoValidMsisdnException;
import com.moxiv.katas.one.KataOneTest;

public class KataOne {

	public String processCommand(final String msisdn, final String command) throws NoValidMsisdnException {


		if (msisdn != null && command.length() > 0) {

			if (msisdn.length() != 11 ) {
				throw new NoValidMsisdnException("msisdn no cumple con el tama�o requerido");
			}else if (command.equalsIgnoreCase("echo") ) 
			{
				return command;
			}else if (command.equalsIgnoreCase("juegos")) 
			{
				return KataOneTest.WELCOME_MESSAGE_PREFIX+" Juegos";
			}
			else if (command.equalsIgnoreCase("horoscopo")) 
			{
				return KataOneTest.WELCOME_MESSAGE_PREFIX+" HOROSCOPO";
			}
			
			else if (command.equalsIgnoreCase("ayuda")) 
			{
				return HELP_MESSAGE;
			}
			else if (command.equalsIgnoreCase("salir horoscopo")) 
			{
				return UNSUBSCRIBED_MESSAGE_PREFIX+" HOROSCOPO";
			}
			else if (command.equalsIgnoreCase("salir juegos")) 
			{
				return  UNSUBSCRIBED_MESSAGE_PREFIX+" JUEGOS";
			}

			int Command_Length = command.length();
			if (Command_Length == 9 || Command_Length == 11) {
				// command could be a msisdn, so we must send a gift
				try {
					Long.parseLong(command);
					// command is a msisdn. we need to send a gift. we need to
					// include the msisdn but in local format
					String msisdn2 = (command.length() == 11) ? command.substring(2) : command;

					return KataOneTest.GIFT_MESSAGE +" "+msisdn2;
				}catch(NumberFormatException e)
				{
					
				}

			}

			return "Comando desconocido. Envie \nayuda\n para ver los comandos disponibles";

		} else if (msisdn == null) {
			throw new NoValidMsisdnException("msisdn no ha sido inicializado");
		}

		return null;
	}

}
